

import java.awt.AWTException;
import java.awt.Color;
import java.awt.Image;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;


public class main_page extends javax.swing.JFrame {
    
    int xMouse;
    int yMouse;
    static TrayIcon trayIcon;
    //JFrame frame;
    
    
    public main_page() {
        this.setIconImage(Toolkit.getDefaultToolkit().getImage("logo.png"));
        initComponents();
        showTrayIcon();
        jLabel3.setVisible(false);
        jLabel4.setVisible(false);
        jLabel5.setVisible(false);
        jLabel10.setVisible(false);
        jLabel7.setVisible(false);
        jLabel8.setVisible(false);
        jLabel9.setVisible(false);
        jLabel13.setVisible(false);
        sp1.setVisible(false);
        ta2.setVisible(false);
        path.setVisible(false);
        b1.setVisible(false);
        b2.setVisible(false);
        sp2.setVisible(false);
        ta4.setVisible(false);
        
        head.setVisible(false);
        
        dsp1.setVisible(false);
        dta2.setVisible(false);
        b3.setVisible(false);
        dta3.setVisible(false);
        db3.setVisible(false);
        dsp2.setVisible(false);
        dta4.setVisible(false);
        db2.setVisible(false);
        ec.setVisible(false);
        dc.setVisible(false);
   }
    
    
    public static String mainstring = "";  
    
    public static void setmainstring(String a)
    {
        mainstring = a;
    }
    
    public static String getmainstring()
    {
        return mainstring;
    }
    
    
    public static String unicode = "";
    
    public static void setunicode(String a)
    {
        unicode = a;
    }
    
    public static String getunicode()
    {
        return unicode;
    }
    
    
    public static String asccode="";
    
    public static void setasccode(String a)
    {
        asccode = a;
    }
    
    public static String getasccode()
    {
        return asccode;
    }
    
    
    public static void fibo(int maxcounter) throws IOException {
        
        int MAX = 100000;
        
        Integer[] one = new Integer[MAX];
        Integer[] two = new Integer[MAX];
        Integer[] three = new Integer[MAX];
        int i,ct1,ct2,ct3,ct,t1,t2,t3,x,y,counter=0;
        
        File fout = new File("enc.txt");
	FileOutputStream fos = new FileOutputStream(fout);
 
	BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
        
        for (i = 0; i < 2; i++) {
		bw.write("1");
		bw.newLine();
	}
        
        for(i=0; i<MAX; i++)
    {
        one[i]=-1;
        two[i]=-1;
        three[i]=-1;
    }
    one[MAX-1]=1;
    two[MAX-1]=1;
    for(; counter<maxcounter-2;)
    {
        ct1=0;
        ct2=0;
        ct3=0;
        i=MAX-1;
        for(;;)
        {
            if(one[i]!=-1)
            {
                i--;
            }
            else
            {
                break;
            }
        }
        ct1=i;
        i=MAX-1;
        for(;;)
        {
            if(two[i]!=-1)
            {
                i--;
            }
            else
            {
                break;
            }
        }
        ct2=i;
        i=MAX-1;
        for(;;)
        {
            if(three[i]!=-1)
            {
                i--;
            }
            else
            {
                break;
            }
        }
        ct3=i;
        ct=ct1>ct2?ct1:ct2;
        i=MAX-1;
        x=0;
        y=0;
        while(i>ct||x>0||y>0)
        {
            if(one[i]>=0)
            {
                t2=one[i];
            }
            else
            {
                t2=0;
            }

            if(two[i]>=0)
            {
                t3=two[i];
            }
            else
            {
                t3=0;
            }
            if(y==1)
            {
                if(x==1)
                {
                    y=1;
                }
                else
                {
                    x=1;
                    y=0;
                }
            }
            t1=t2+t3+x;
            x=0;
            if(t1<=9)
            {
                three[i]=t1;
            }
            else
            {
                t1=t1%10;
                three[i]=t1;
                if(two[i-1]+1<=9)
                {
                    if(two[i-1]==-1)
                    {
                        x=1;
                    }
                    else
                    {
                        x=1;
                    }
                }
                else
                {
                    if(one[i-1]+1<=9)
                    {
                        if(one[i-1]==-1)
                        {
                            x=1;
                        }
                        else
                        {
                            x=1;
                        }
                    }
                    else
                    {
                        x=0;
                        y=1;
                    }
                }
            }
            i--;
        }
        if(three[0]!=-1)
        {
            break;
        }
        i=MAX-1;
        for(;;)
        {
            if(two[i]!=-1)
            {
                i--;
            }
            else
            {
                break;
            }
        }
        ct2=i;
        for(i=ct2+1; i<MAX; i++)
        {
            one[i]=two[i];
        }
        i=MAX-1;
        for(;;)
        {
            if(three[i]!=-1)
            {
                i--;
            }
            else
            {
                break;
            }
        }
        ct3=i;
        for(i=ct3+1; i<MAX; i++)
        {
            two[i]=three[i];
        }
        String s;
        for(i=ct3+1; i<MAX; i++)
        {
            s = Integer.toString(three[i]);
            bw.write(s);
            
        }
        bw.newLine();
        counter++;

    }
    bw.close();
}
    
    
  
    public static void showTrayIcon()
    {
        
          if(!SystemTray.isSupported())
          {
              System.exit(0);
              return;
          }
          
          final PopupMenu popup = new PopupMenu();
          trayIcon = new TrayIcon(CreateIcon("logo.png","Tray Icon"));
          final SystemTray tray = SystemTray.getSystemTray();
          trayIcon.setToolTip("Unicode Encryption\nVersion 1.6.25");
          //Add component / Menu Items
          MenuItem EncryptionItem = new MenuItem("Encrypt");
          MenuItem DecryptionItem = new MenuItem("Decrypt");
          MenuItem ExitItem = new MenuItem("Exit");
          //populate the pop up menu
          popup.add(EncryptionItem);
          popup.add(DecryptionItem);
          popup.addSeparator();
          popup.add(ExitItem);
          
          trayIcon.setPopupMenu(popup);
          
          ExitItem.addActionListener((ActionEvent e) -> 
          {
              tray.remove(trayIcon);
              System.exit(0);
          });
                  
          
          try
          {
              tray.add(trayIcon);
          }
          catch(AWTException e)
          {
              
          }
    }
    
    protected static Image CreateIcon(String path, String desc)
    {
        URL ImageURL = main_page.class.getResource(path);
        return (new ImageIcon(ImageURL,desc)).getImage();
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        ta2 = new javax.swing.JTextField();
        dta2 = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        ec = new javax.swing.JButton();
        dc = new javax.swing.JButton();
        db2 = new javax.swing.JButton();
        db3 = new javax.swing.JButton();
        b3 = new javax.swing.JButton();
        dta3 = new javax.swing.JTextField();
        path = new javax.swing.JTextField();
        sp1 = new javax.swing.JScrollPane();
        ta1 = new javax.swing.JTextArea();
        dsp1 = new javax.swing.JScrollPane();
        dta1 = new javax.swing.JTextArea();
        sp2 = new javax.swing.JScrollPane();
        ta4 = new javax.swing.JTextArea();
        dsp2 = new javax.swing.JScrollPane();
        dta4 = new javax.swing.JTextArea();
        jLabel2 = new javax.swing.JLabel();
        b2 = new javax.swing.JButton();
        jLabel11 = new javax.swing.JLabel();
        b1 = new javax.swing.JButton();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        head = new javax.swing.JLabel();
        miniB = new javax.swing.JLabel();
        closeB = new javax.swing.JLabel();
        framedrag = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(670, 442));
        setUndecorated(true);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowActivated(java.awt.event.WindowEvent evt) {
                formWindowActivated(evt);
            }
        });
        getContentPane().setLayout(null);

        ta2.setBackground(new java.awt.Color(204, 204, 204));
        ta2.setForeground(new java.awt.Color(255, 255, 255));
        ta2.setCaretColor(new java.awt.Color(255, 255, 255));
        ta2.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        ta2.setOpaque(false);
        ta2.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                ta2FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                ta2FocusLost(evt);
            }
        });
        getContentPane().add(ta2);
        ta2.setBounds(440, 190, 210, 30);

        dta2.setForeground(new java.awt.Color(255, 255, 255));
        dta2.setCaretColor(new java.awt.Color(255, 255, 255));
        dta2.setOpaque(false);
        dta2.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                dta2FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                dta2FocusLost(evt);
            }
        });
        getContentPane().add(dta2);
        dta2.setBounds(440, 190, 210, 30);

        jLabel10.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel10.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel10MouseClicked(evt);
            }
        });
        getContentPane().add(jLabel10);
        jLabel10.setBounds(60, 180, 50, 20);

        jLabel9.setForeground(new java.awt.Color(255, 255, 0));
        jLabel9.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        getContentPane().add(jLabel9);
        jLabel9.setBounds(110, 120, 70, 35);

        jLabel8.setForeground(new java.awt.Color(255, 0, 0));
        jLabel8.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        getContentPane().add(jLabel8);
        jLabel8.setBounds(120, 80, 60, 23);

        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/help.png"))); // NOI18N
        getContentPane().add(jLabel7);
        jLabel7.setBounds(60, 60, 120, 100);

        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel6MouseClicked(evt);
            }
        });
        getContentPane().add(jLabel6);
        jLabel6.setBounds(60, 20, 40, 20);

        jLabel5.setForeground(new java.awt.Color(255, 255, 0));
        jLabel5.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel5MouseClicked(evt);
            }
        });
        getContentPane().add(jLabel5);
        jLabel5.setBounds(60, 130, 100, 30);

        jLabel4.setForeground(new java.awt.Color(204, 0, 0));
        jLabel4.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel4MouseClicked(evt);
            }
        });
        getContentPane().add(jLabel4);
        jLabel4.setBounds(60, 90, 100, 20);

        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/menu.png"))); // NOI18N
        jLabel3.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jLabel3FocusLost(evt);
            }
        });
        getContentPane().add(jLabel3);
        jLabel3.setBounds(20, 60, 140, 150);

        ec.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        ec.setForeground(new java.awt.Color(255, 255, 255));
        ec.setIcon(new javax.swing.ImageIcon(getClass().getResource("/clear_Icon1.png"))); // NOI18N
        ec.setText("Clear");
        ec.setBorder(null);
        ec.setBorderPainted(false);
        ec.setContentAreaFilled(false);
        ec.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        ec.setFocusPainted(false);
        ec.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                ecMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                ecMouseExited(evt);
            }
        });
        ec.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ecActionPerformed(evt);
            }
        });
        getContentPane().add(ec);
        ec.setBounds(580, 370, 80, 40);

        dc.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        dc.setForeground(new java.awt.Color(255, 255, 255));
        dc.setIcon(new javax.swing.ImageIcon(getClass().getResource("/clear_Icon1.png"))); // NOI18N
        dc.setText("Clear");
        dc.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        dc.setBorderPainted(false);
        dc.setContentAreaFilled(false);
        dc.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        dc.setFocusPainted(false);
        dc.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                dcMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                dcMouseExited(evt);
            }
        });
        dc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dcActionPerformed(evt);
            }
        });
        getContentPane().add(dc);
        dc.setBounds(580, 370, 80, 40);

        db2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        db2.setForeground(new java.awt.Color(255, 255, 255));
        db2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/rcv messsage.png"))); // NOI18N
        db2.setText("Receive Message");
        db2.setToolTipText("");
        db2.setBorder(null);
        db2.setBorderPainted(false);
        db2.setContentAreaFilled(false);
        db2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        db2.setFocusPainted(false);
        db2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                db2MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                db2MouseExited(evt);
            }
        });
        db2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                db2ActionPerformed(evt);
            }
        });
        getContentPane().add(db2);
        db2.setBounds(430, 320, 180, 70);

        db3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        db3.setForeground(new java.awt.Color(255, 255, 255));
        db3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/open_file1.png"))); // NOI18N
        db3.setText("Open");
        db3.setBorder(null);
        db3.setBorderPainted(false);
        db3.setContentAreaFilled(false);
        db3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        db3.setFocusPainted(false);
        db3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                db3MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                db3MouseExited(evt);
            }
        });
        db3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                db3ActionPerformed(evt);
            }
        });
        getContentPane().add(db3);
        db3.setBounds(370, 250, 90, 40);

        b3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        b3.setForeground(new java.awt.Color(255, 255, 255));
        b3.setText("Extract");
        b3.setContentAreaFilled(false);
        b3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        b3.setFocusPainted(false);
        b3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                b3MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                b3MouseExited(evt);
            }
        });
        b3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b3ActionPerformed(evt);
            }
        });
        getContentPane().add(b3);
        b3.setBounds(430, 120, 90, 20);

        dta3.setForeground(new java.awt.Color(255, 255, 255));
        dta3.setCaretColor(new java.awt.Color(255, 255, 255));
        dta3.setOpaque(false);
        getContentPane().add(dta3);
        dta3.setBounds(70, 260, 290, 30);

        path.setBackground(new java.awt.Color(204, 204, 204));
        path.setForeground(new java.awt.Color(255, 255, 255));
        path.setCaretColor(new java.awt.Color(255, 255, 255));
        path.setOpaque(false);
        getContentPane().add(path);
        path.setBounds(70, 240, 350, 30);

        sp1.setBackground(new java.awt.Color(204, 204, 204));
        sp1.setEnabled(false);
        sp1.setOpaque(false);

        ta1.setBackground(new java.awt.Color(204, 204, 204));
        ta1.setColumns(20);
        ta1.setLineWrap(true);
        ta1.setRows(5);
        ta1.setText("Enter Your Message");
        ta1.setOpaque(false);
        ta1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                ta1FocusGained(evt);
            }
        });
        sp1.setViewportView(ta1);

        getContentPane().add(sp1);
        sp1.setBounds(70, 120, 350, 100);

        dsp1.setEnabled(false);
        dsp1.setOpaque(false);

        dta1.setColumns(20);
        dta1.setLineWrap(true);
        dta1.setRows(5);
        dta1.setText("Encrypted Message");
        dta1.setOpaque(false);
        dta1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                dta1FocusGained(evt);
            }
        });
        dsp1.setViewportView(dta1);

        getContentPane().add(dsp1);
        dsp1.setBounds(70, 120, 350, 100);

        sp2.setBackground(new java.awt.Color(204, 204, 204));
        sp2.setEnabled(false);
        sp2.setOpaque(false);

        ta4.setBackground(new java.awt.Color(204, 204, 204));
        ta4.setColumns(20);
        ta4.setLineWrap(true);
        ta4.setRows(5);
        ta4.setText("Encrypted Message");
        ta4.setOpaque(false);
        sp2.setViewportView(ta4);

        getContentPane().add(sp2);
        sp2.setBounds(70, 290, 350, 100);

        dsp2.setBackground(new java.awt.Color(204, 204, 204));
        dsp2.setEnabled(false);
        dsp2.setOpaque(false);

        dta4.setBackground(new java.awt.Color(204, 204, 204));
        dta4.setColumns(20);
        dta4.setLineWrap(true);
        dta4.setRows(5);
        dta4.setText("Your Message");
        dta4.setOpaque(false);
        dsp2.setViewportView(dta4);

        getContentPane().add(dsp2);
        dsp2.setBounds(70, 310, 350, 100);

        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel2MouseClicked(evt);
            }
        });
        getContentPane().add(jLabel2);
        jLabel2.setBounds(0, 20, 50, 20);

        b2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        b2.setForeground(new java.awt.Color(255, 255, 255));
        b2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Ok-logo1.png"))); // NOI18N
        b2.setText("OK");
        b2.setBorder(null);
        b2.setContentAreaFilled(false);
        b2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        b2.setFocusPainted(false);
        b2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                b2MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                b2MouseExited(evt);
            }
        });
        b2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b2ActionPerformed(evt);
            }
        });
        getContentPane().add(b2);
        b2.setBounds(440, 320, 70, 40);

        jLabel11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/names_design.png"))); // NOI18N
        getContentPane().add(jLabel11);
        jLabel11.setBounds(0, 330, 640, 80);

        b1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        b1.setForeground(new java.awt.Color(255, 255, 255));
        b1.setText("Attach");
        b1.setContentAreaFilled(false);
        b1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        b1.setFocusPainted(false);
        b1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                b1MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                b1MouseExited(evt);
            }
        });
        b1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b1ActionPerformed(evt);
            }
        });
        getContentPane().add(b1);
        b1.setBounds(420, 245, 80, 20);

        jLabel12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/main_logo.png"))); // NOI18N
        getContentPane().add(jLabel12);
        jLabel12.setBounds(470, 200, 200, 130);

        jLabel13.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(255, 255, 255));
        jLabel13.setText("Enter a key");
        getContentPane().add(jLabel13);
        jLabel13.setBounds(440, 170, 120, 20);

        head.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        head.setForeground(new java.awt.Color(255, 255, 255));
        head.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        head.setText("Encryption");
        getContentPane().add(head);
        head.setBounds(190, 60, 280, 30);

        miniB.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        miniB.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                miniBMouseClicked(evt);
            }
        });
        getContentPane().add(miniB);
        miniB.setBounds(613, 7, 20, 20);

        closeB.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        closeB.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                closeBMouseClicked(evt);
            }
        });
        getContentPane().add(closeB);
        closeB.setBounds(647, 7, 20, 20);

        framedrag.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                framedragMouseDragged(evt);
            }
        });
        framedrag.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                framedragMousePressed(evt);
            }
        });
        getContentPane().add(framedrag);
        framedrag.setBounds(0, 0, 590, 18);

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/homepage.png"))); // NOI18N
        jLabel1.setMaximumSize(new java.awt.Dimension(681, 381));
        jLabel1.setMinimumSize(new java.awt.Dimension(677, 381));
        jLabel1.setName(""); // NOI18N
        jLabel1.setOpaque(true);
        jLabel1.setPreferredSize(new java.awt.Dimension(681, 381));
        jLabel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel1MouseClicked(evt);
            }
        });
        getContentPane().add(jLabel1);
        jLabel1.setBounds(0, 0, 677, 443);

        setSize(new java.awt.Dimension(677, 441));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowActivated(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowActivated
        // TODO add your handling code here:
        
    }//GEN-LAST:event_formWindowActivated

    private void closeBMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_closeBMouseClicked
        System.exit(0);
    }//GEN-LAST:event_closeBMouseClicked

    private void miniBMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_miniBMouseClicked
        
        this.setState(main_page.ICONIFIED);
    }//GEN-LAST:event_miniBMouseClicked

    private void framedragMouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_framedragMouseDragged
        
        int x = evt.getXOnScreen();
        int y = evt.getYOnScreen();
        
        this.setLocation(x - xMouse ,y - yMouse);
    }//GEN-LAST:event_framedragMouseDragged

    private void framedragMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_framedragMousePressed
        
        xMouse = evt.getX();
        yMouse = evt.getY();
    }//GEN-LAST:event_framedragMousePressed

    int c = 0;
    int d = 0;
    
    private void jLabel2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel2MouseClicked
        // TODO add your handling code here:
        if(c==0)
        {
            jLabel11.setVisible(true);
            jLabel12.setVisible(true);
            jLabel3.setVisible(true);
            jLabel4.setVisible(true);
            jLabel5.setVisible(true);
            jLabel10.setVisible(true);
            
            sp1.setVisible(false);
            ta2.setVisible(false);
            path.setVisible(false);
            b1.setVisible(false);
            b2.setVisible(false);
            jLabel13.setVisible(false);
            c=1;
            jLabel7.setVisible(false);
            jLabel8.setVisible(false);
            jLabel9.setVisible(false);
            d=0;
            head.setVisible(false);
            sp2.setVisible(false);
            ta4.setVisible(false);
            
            dsp1.setVisible(false);
            dta2.setVisible(false);
            b3.setVisible(false);
            dta3.setVisible(false);
            db3.setVisible(false);
            dsp2.setVisible(false);
            dta4.setVisible(false);
            db2.setVisible(false);
            ec.setVisible(false);
            dc.setVisible(false);
        }
        else
        {
            jLabel3.setVisible(false);
            jLabel4.setVisible(false);
            jLabel5.setVisible(false);
            jLabel10.setVisible(false);
            c=0;
        }
    }//GEN-LAST:event_jLabel2MouseClicked

    private void jLabel6MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel6MouseClicked
        // TODO add your handling code here:
        if(d==0)
        {
            jLabel7.setVisible(true);
            jLabel8.setVisible(true);
            jLabel9.setVisible(true);
            jLabel11.setVisible(true);
            jLabel12.setVisible(true);
            d=1;
            jLabel13.setVisible(false);
            jLabel3.setVisible(false);
            jLabel4.setVisible(false);
            jLabel5.setVisible(false);
            jLabel10.setVisible(false);
            sp1.setVisible(false);
            ta2.setVisible(false);
            path.setVisible(false);
            b1.setVisible(false);
            b2.setVisible(false);
            c=0;
            head.setVisible(false);
            sp2.setVisible(false);
            ta4.setVisible(false);
            
            dsp1.setVisible(false);
            dta2.setVisible(false);
            b3.setVisible(false);
            dta3.setVisible(false);
            db3.setVisible(false);
            dsp2.setVisible(false);
            dta4.setVisible(false);
            db2.setVisible(false);
            ec.setVisible(false);
            dc.setVisible(false);
        }
        else
        {
            jLabel7.setVisible(false);
            jLabel8.setVisible(false);
            jLabel9.setVisible(false);
            d=0;
        }
    }//GEN-LAST:event_jLabel6MouseClicked

    private void jLabel4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel4MouseClicked
        // TODO add your handling code here:
        head.setVisible(true);
        head.setText("Encryption");
        jLabel11.setVisible(false);
        jLabel12.setVisible(false);
        jLabel3.setVisible(false);
        jLabel4.setVisible(false);
        jLabel5.setVisible(false);
        jLabel10.setVisible(false);
        c=0;
        sp1.setVisible(true);
        sp1.setBackground(new Color(0,0,0,0));
        ta1.setVisible(true);
        ta1.setOpaque(false);
        ta1.setBackground(new Color(0,0,0,0));
        ta2.setVisible(true);
        ta2.setOpaque(false);
        ta2.setBackground(new Color(0,0,0,0));
        path.setVisible(true);
        path.setOpaque(false);
        path.setBackground(new Color(0,0,0,0));
        b1.setVisible(true);
        b2.setVisible(true);
        jLabel13.setVisible(true);
        sp2.setVisible(true);
        ta4.setVisible(true);
        
        dsp1.setVisible(false);
        jLabel13.setText("Enter a key");
        dta2.setVisible(false);
        b3.setVisible(false);
        dta3.setVisible(false);
        db3.setVisible(false);
        dsp2.setVisible(false);
        dta4.setVisible(false);
        db2.setVisible(false);
        ec.setVisible(true);
        dc.setVisible(false);
    }//GEN-LAST:event_jLabel4MouseClicked

    private void jLabel10MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel10MouseClicked
        // TODO add your handling code here:
        System.exit(0);
    }//GEN-LAST:event_jLabel10MouseClicked

    private void ta2FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_ta2FocusGained
        // TODO add your handling code here:
        ta2.setText("");
        //ta4.setText("");

    }//GEN-LAST:event_ta2FocusGained

    private void ta2FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_ta2FocusLost
        // TODO add your handling code here:
        if(!"".equals(ta2.getText()))
        {
        String s = ta2.getText();
        String s1 = "" + s.charAt(0);
        ta2.setText(s1);
        }
        else
        {
            
        }
    }//GEN-LAST:event_ta2FocusLost

    private void jLabel3FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jLabel3FocusLost
        // TODO add your handling code here:
        jLabel3.setVisible(false);
        jLabel4.setVisible(false);
        jLabel5.setVisible(false);
        jLabel10.setVisible(false);
        c=0;
    }//GEN-LAST:event_jLabel3FocusLost

    private void b2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b2ActionPerformed
        
        try {
            
            String[] alphabets = new String[96];//array for storing ascii value of upercase
            for(int i=0;i<96;i++)
                alphabets[i]=String.valueOf(Character.toChars(32+i));
         if(!"".equals(ta1.getText()))
         {
            String a = ta1.getText();
            
        fibo(a.length());
        
        if(!"".equals(ta2.getText()))
        {
            ta4.setText("");
            
            String key = ta2.getText();
            
            
            int position=0;
        
        while(!key.equals(alphabets[position]))//find the position of key in upercase string
        {
            position++;
        }
        
        Long[] fibonacci = new Long[a.length()];
        
        if(a.length()==1)
        {
            FileInputStream fstream = new FileInputStream("enc.txt");
        BufferedReader br = new BufferedReader(new InputStreamReader(fstream));

        String strLine;
        int f=0;
        strLine = br.readLine() ;
            fibonacci[f] = Long.parseLong(strLine);
            f++;
        
        br.close();
        }
        
        else if(a.length()<=92)
        {
         
        FileInputStream fstream = new FileInputStream("enc.txt");
        BufferedReader br = new BufferedReader(new InputStreamReader(fstream));

        String strLine;
        int f=0;
        while ((strLine = br.readLine()) != null)   {
            fibonacci[f] = Long.parseLong(strLine);
            f++;
        }
        br.close();
        }
        
        
        else
        {
            
            Long[] copy = new Long[92];
            
            FileInputStream fstream = new FileInputStream("enc.txt");
        BufferedReader br = new BufferedReader(new InputStreamReader(fstream));

        String strLine;
        int f=0;
        while (f!=92)   {
            strLine = br.readLine();
            copy[f] = Long.parseLong(strLine);
            f++;
        }
        br.close();
            int k=0;
            int y = a.length()/92;
            
            String s1;
            BufferedWriter out1 =  new BufferedWriter(new FileWriter("enc.txt"));
                for(int j=0;j<92;j++)
                {
                    s1 = Long.toString(copy[j]);
                    out1.write(s1);
                    out1.newLine();
                }
            k++;
            out1.close();
            
            
            while(k<=y)
            {
                String s2;
            BufferedWriter out2 =  new BufferedWriter(new FileWriter("enc.txt",true));
                for(int j=0;j<92;j++)
                {
                    s2 = Long.toString(copy[j]);
                    out2.write(s2);
                    out2.newLine();
                }
            k++;
            out2.close();
            }
            
            //Long[] fibonacci = new Long[a.length()];
        // Open the file
        
        
        FileInputStream fstream1 = new FileInputStream("enc.txt");
        BufferedReader br2 = new BufferedReader(new InputStreamReader(fstream1));

        String strLine1;
        int l=0;
        while (l<a.length())   {
            strLine1 = br2.readLine();
            fibonacci[l] = Long.parseLong(strLine1);
            l++;
        }
        br.close();
              
        }
        
        //abcdefghijklmnopqrstuvwxyz
        Integer[] cipher_position = new Integer[a.length()];
        for(int i=0; i<a.length(); i++){
            //System.out.println(fibonacci(i));
            cipher_position[i] = (int) ((fibonacci[i]+ Long.valueOf(position)-1)%96);//call the function fibonacci and add the position of key and do -1 bcs our fibonacci starts from 1 and we want to take the position of key also for calculating the cipher text position from array alphabets
            //System.out.println(cipher_position[i-2]);
        }
        
        String[] cipher_text = new String[a.length()];
        for(int i=0; i<a.length(); i++)
        {
            cipher_text[i]=alphabets[cipher_position[i]];//storing the cipher text into cipher_text array
        }
            
        
        
        //String a = ta1.getText();
        
        Integer[] ascii_massage = new Integer[a.length()];//array for storing ascii value of massage
        
        for(int i=0;i<a.length();i++)
            ascii_massage[i]=(int) a.charAt(i);
        
        
        
        Integer[] cipher_integer = new Integer[a.length()];
        for(int i=0;i<a.length();i++){
            cipher_integer[i] = 5*((int) cipher_text[i].charAt(0))+ascii_massage[i];
             //System.out.println(cipher_integer[i]);
        }
        
        String[] unicode = new String[a.length()];
        unicode[0] = String.valueOf(Character.toChars(cipher_integer[0]));
        String u1 = unicode[0];
        
        for(int i=1;i<a.length();i++){
            unicode[i] = String.valueOf(Character.toChars(cipher_integer[i]));
             u1 += unicode[i];
        }
        
        setunicode(u1);
        
        ta4.setText(u1);
        
        option o = new option();
        o.setVisible(true);
        
        File f = new File("enc.txt");
            f.delete();
         }
        else
         {
             JOptionPane.showMessageDialog(null, "Please Enter Key!!!");
         }
         }
         
         else
         {
             JOptionPane.showMessageDialog(null, "Please Enter Any Message!!!");
         }
        } catch (IOException ex) {
            Logger.getLogger(main_page.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }//GEN-LAST:event_b2ActionPerformed

    private void jLabel5MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel5MouseClicked
        // TODO add your handling code here:
        jLabel11.setVisible(false);
        jLabel12.setVisible(false);
        jLabel3.setVisible(false);
        jLabel4.setVisible(false);
        jLabel5.setVisible(false);
        jLabel10.setVisible(false);
        c=0;
        
        sp1.setVisible(false);
        ta1.setVisible(false);
        ta2.setVisible(false);
        path.setVisible(false);
        b1.setVisible(false);
        b2.setVisible(false);
        jLabel13.setVisible(false);
        sp2.setVisible(false);
        ta4.setVisible(false);
        
        head.setVisible(true);
        head.setText("Decryption");
        dsp1.setVisible(true);
        dsp1.setBackground(new Color(0,0,0,0));
        dta1.setVisible(true);
        dta1.setOpaque(false);
        dta1.setBackground(new Color(0,0,0,0));
        jLabel13.setVisible(true);
        jLabel13.setText("Enter the key");
        dta2.setVisible(true);
        dta2.setOpaque(false);
        dta2.setBackground(new Color(0,0,0,0));
        b3.setVisible(true);
        dta3.setVisible(true);
        dta3.setOpaque(false);
        dta3.setBackground(new Color(0,0,0,0));
        db3.setVisible(true);
        dsp2.setVisible(true);
        dsp2.setBackground(new Color(0,0,0,0));
        dta4.setVisible(true);
        dta4.setOpaque(false);
        dta4.setBackground(new Color(0,0,0,0));
        db2.setVisible(true);
        
        ec.setVisible(false);
        dc.setVisible(true);
    }//GEN-LAST:event_jLabel5MouseClicked

    private void dta2FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_dta2FocusGained
        // TODO add your handling code here:
        dta2.setText("");
    }//GEN-LAST:event_dta2FocusGained

    private void dta2FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_dta2FocusLost
        // TODO add your handling code here:
        String s = dta2.getText();
        char c = s.charAt(0);
        String s1 = "" + c;
        dta2.setText(s1);
    }//GEN-LAST:event_dta2FocusLost

    private void ta1FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_ta1FocusGained
        // TODO add your handling code here:
        ta1.setText("");
    }//GEN-LAST:event_ta1FocusGained

    private void b1MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_b1MouseEntered
        // TODO add your handling code here:
        String fp = "<html><u>Attach</u></html>";
        b1.setText(fp);
        b1.setForeground(Color.YELLOW);
    }//GEN-LAST:event_b1MouseEntered

    private void b1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_b1MouseExited
        // TODO add your handling code here:
        String fp = "Attach";
        b1.setText(fp);
        b1.setForeground(Color.WHITE);
    }//GEN-LAST:event_b1MouseExited

    private void b2MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_b2MouseEntered
        // TODO add your handling code here:
        String fp = "<html><u>OK</u></html>";
        b2.setText(fp);
        b2.setForeground(Color.YELLOW);
    }//GEN-LAST:event_b2MouseEntered

    private void b2MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_b2MouseExited
        // TODO add your handling code here:
        String fp = "OK";
        b2.setText(fp);
        b2.setForeground(Color.WHITE);
    }//GEN-LAST:event_b2MouseExited

    private void b3MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_b3MouseEntered
        // TODO add your handling code here:
        String fp = "<html><u>Extract</u></html>";
        b3.setText(fp);
        b3.setForeground(Color.YELLOW);
    }//GEN-LAST:event_b3MouseEntered

    private void b3MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_b3MouseExited
        // TODO add your handling code here:
        String fp = "Extract";
        b3.setText(fp);
        b3.setForeground(Color.WHITE);
    }//GEN-LAST:event_b3MouseExited

    private void db3MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_db3MouseExited
        // TODO add your handling code here:
        String fp = "Open";
        db3.setText(fp);
        db3.setForeground(Color.WHITE);
    }//GEN-LAST:event_db3MouseExited

    private void db3MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_db3MouseEntered
        // TODO add your handling code here:
        String fp = "<html><u>Open</u></html>";
        db3.setText(fp);
        db3.setForeground(Color.YELLOW);
    }//GEN-LAST:event_db3MouseEntered

    private void db2MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_db2MouseEntered
        // TODO add your handling code here:
        String fp = "<html><u>Receive Message</u></html>";
        db2.setText(fp);
        db2.setForeground(Color.YELLOW);
    }//GEN-LAST:event_db2MouseEntered

    private void db2MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_db2MouseExited
        // TODO add your handling code here:
        String fp = "Receive Message";
        db2.setText(fp);
        db2.setForeground(Color.WHITE);
    }//GEN-LAST:event_db2MouseExited
   int o = 0; 
   String filename;
    private void b1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b1ActionPerformed
       
        FileReader filereader = null;
        
        try {
           
            
            if(!"".equals(ta2.getText()))
            {
                o=1;
                FileFilter filter = new FileNameExtensionFilter("Text File","txt");
            JFileChooser chooser = new JFileChooser(System.getProperty("user.home") + "/Desktop");
            chooser.addChoosableFileFilter(filter);
            int returnval = chooser.showOpenDialog(null);
            
            File f1 = chooser.getSelectedFile();
            filename = f1.getAbsolutePath();
            path.setText(filename);
            
            String text = null;
            String line = null;
            if(returnval == JFileChooser.APPROVE_OPTION)
            {
                File myFile = chooser.getSelectedFile();
                text = myFile + "";
            }
            
            File myfile = new File(text);
            filereader = new FileReader(myfile);
            BufferedReader reader = new BufferedReader(filereader);
            
            String s = reader.readLine() + "\n";
            
            while((line = reader.readLine())!=null)
            {
                s = s + line + "\n";
            }
            
            String[] alphabets = new String[96];//array for storing ascii value of upercase
            for(int i=0;i<96;i++)
                alphabets[i]=String.valueOf(Character.toChars(32+i));
            
            String a = s;
            
            fibo(a.length());
            
            String key = ta2.getText();
                
                int position=0;
                
                while(!key.equals(alphabets[position]))//find the position of key in upercase string
                {
                    position++;
                }
                
                Long[] fibonacci = new Long[a.length()];
                
                if(a.length()==1)
                {
                    FileInputStream fstream = new FileInputStream("enc.txt");
                    BufferedReader br = new BufferedReader(new InputStreamReader(fstream));

                    String strLine;
                    int f=0;
                    strLine = br.readLine() ;
                    fibonacci[f] = Long.parseLong(strLine);
                    f++;
        
                    br.close();
                }
                else if(a.length()<=92)
                {
                    
                    FileInputStream fstream = new FileInputStream("enc.txt");
                    BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
                    
                    String strLine;
                    int f=0;
                    while ((strLine = br.readLine()) != null)   {
                        fibonacci[f] = Long.parseLong(strLine);
                        f++;
                    }
                    br.close();
                }
                
                
                else
                {
                    
                    Long[] copy = new Long[92];
                    
                    FileInputStream fstream = new FileInputStream("enc.txt");
                    BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
                    
                    String strLine;
                    int f=0;
                    while (f!=92)   {
                        strLine = br.readLine();
                        copy[f] = Long.parseLong(strLine);
                        f++;
                    }
                    br.close();
                    int k=0;
                    int y = a.length()/92;
                    
                    String s1;
                    BufferedWriter out1 =  new BufferedWriter(new FileWriter("enc.txt"));
                    for(int j=0;j<92;j++)
                    {
                        s1 = Long.toString(copy[j]);
                        out1.write(s1);
                        out1.newLine();
                    }
                    k++;
                    out1.close();
                    
                    
                    while(k<=y)
                    {
                        String s2;
                        BufferedWriter out2 =  new BufferedWriter(new FileWriter("enc.txt",true));
                        for(int j=0;j<92;j++)
                        {
                            s2 = Long.toString(copy[j]);
                            out2.write(s2);
                            out2.newLine();
                        }
                        k++;
                        out2.close();
                    }
                    
                    //Long[] fibonacci = new Long[a.length()];
                    // Open the file
                    
                    
                    FileInputStream fstream1 = new FileInputStream("enc.txt");
                    BufferedReader br2 = new BufferedReader(new InputStreamReader(fstream1));
                    
                    String strLine1;
                    int l=0;
                    while (l<a.length())   {
                        strLine1 = br2.readLine();
                        fibonacci[l] = Long.parseLong(strLine1);
                        l++;
                    }
                    br.close();
                    
                }
                
                //abcdefghijklmnopqrstuvwxyz
                Integer[] cipher_position = new Integer[a.length()];
                for(int i=0; i<a.length(); i++){
                    //System.out.println(fibonacci(i));
                    cipher_position[i] = (int) ((fibonacci[i]+ Long.valueOf(position)-1)%96);//call the function fibonacci and add the position of key and do -1 bcs our fibonacci starts from 1 and we want to take the position of key also for calculating the cipher text position from array alphabets
                    //System.out.println(cipher_position[i-2]);
                }
                
                String[] cipher_text = new String[a.length()];
                for(int i=0; i<a.length(); i++)
                {
                    cipher_text[i]=alphabets[cipher_position[i]];//storing the cipher text into cipher_text array
                }
                
                
                
                //String a = ta1.getText();
                
                Integer[] ascii_massage = new Integer[a.length()];//array for storing ascii value of massage
                
                for(int i=0;i<a.length();i++)
                    ascii_massage[i]=(int) a.charAt(i);
                
                
                
                Integer[] cipher_integer = new Integer[a.length()];
                for(int i=0;i<a.length();i++){
                    cipher_integer[i] = 5*((int) cipher_text[i].charAt(0))+ascii_massage[i];
                    //System.out.println(cipher_integer[i]);
                }
                
                String[] unicode = new String[a.length()];
                unicode[0] = String.valueOf(Character.toChars(cipher_integer[0]));
                String u1 = unicode[0];
                
                for(int i=1;i<a.length();i++){
                    unicode[i] = String.valueOf(Character.toChars(cipher_integer[i]));
                    u1 += unicode[i];
                }
                
                
                setmainstring(u1.substring(0, u1.length()-1));
                
                
                File f = new File("enc.txt");
                f.delete();
            }
            
            else{
                JOptionPane.showMessageDialog(null,"Please Enter Key First!!!");
            }
        } catch (IOException ex) {
            Logger.getLogger(main_page.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_b1ActionPerformed

    private void ecMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ecMouseEntered
        // TODO add your handling code here:
        String fp = "<html><u>Clear</u></html>";
        ec.setText(fp);
        ec.setForeground(Color.YELLOW);
    }//GEN-LAST:event_ecMouseEntered

    private void ecMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ecMouseExited
        // TODO add your handling code here:
        String fp = "Clear";
        ec.setText(fp);
        ec.setForeground(Color.WHITE);
    }//GEN-LAST:event_ecMouseExited

    private void dcMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_dcMouseEntered
        // TODO add your handling code here:
        String fp = "<html><u>Clear</u></html>";
        dc.setText(fp);
        dc.setForeground(Color.YELLOW);
    }//GEN-LAST:event_dcMouseEntered

    private void ecActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ecActionPerformed
        // TODO add your handling code here:
        
        ta1.setText("");
        ta2.setText("");
        path.setText("");
        ta4.setText("");
        
    }//GEN-LAST:event_ecActionPerformed

    private void dcActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dcActionPerformed
        // TODO add your handling code here:
        
        dta1.setText("");
        dta2.setText("");
        dta3.setText("");
        dta4.setText("");
        
    }//GEN-LAST:event_dcActionPerformed

    private void dcMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_dcMouseExited
        // TODO add your handling code here:
        String fp = "Clear";
        dc.setText(fp);
        dc.setForeground(Color.WHITE);
        
    }//GEN-LAST:event_dcMouseExited

    private void b3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b3ActionPerformed
        // TODO add your handling code here:
        if(!"".equals(option.getunicode())){
        dta1.setText(option.getunicode());
        
        }
        else
        {
            JOptionPane.showMessageDialog(null, "No Message is selected!!!");
        }
        
        if(o==1){
        dta3.setText(filename);
        
        }
        
        
        
    }//GEN-LAST:event_b3ActionPerformed

    private void db3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_db3ActionPerformed
        // TODO add your handling code here:
        
        if(!"".equals(dta3.getText()) && o==1)
        {
            
            String s = getmainstring();
            try {
                // TODO add your handling code here:
                
                String[] alphabets = new String[96];//array for storing ascii value of upercase
                for(int i=0;i<96;i++)
                    alphabets[i]=String.valueOf(Character.toChars(32+i));
                
                String a = s;
                
                fibo(a.length());
                
                if( !"".equals(dta2.getText()))
                {
                    String key = dta2.getText();
                    
                    int position=0;
                    
                    while(!key.equals(alphabets[position]))//find the position of key in upercase string
                    {
                        position++;
                    }
                    
                    Long[] fibonacci = new Long[a.length()];
                    
                    if(a.length()==1)
        {
            FileInputStream fstream = new FileInputStream("enc.txt");
        BufferedReader br = new BufferedReader(new InputStreamReader(fstream));

        String strLine;
        int f=0;
        strLine = br.readLine() ;
            fibonacci[f] = Long.parseLong(strLine);
            f++;
        
        br.close();
        }
                    else if(a.length()<=92)
                    {
                        
                        FileInputStream fstream = new FileInputStream("enc.txt");
                        BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
                        
                        String strLine;
                        int f=0;
                        while ((strLine = br.readLine()) != null)   {
                            fibonacci[f] = Long.parseLong(strLine);
                            f++;
                        }
                        br.close();
                    }
                    
                    
                    else
                    {
                        
                        Long[] copy = new Long[92];
                        
                        FileInputStream fstream = new FileInputStream("enc.txt");
                        BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
                        
                        String strLine;
                        int f=0;
                        while (f!=92)   {
                            strLine = br.readLine();
                            copy[f] = Long.parseLong(strLine);
                            f++;
                        }
                        br.close();
                        int k=0;
                        int y = a.length()/92;
                        
                        String s1;
                        BufferedWriter out1 =  new BufferedWriter(new FileWriter("enc.txt"));
                        for(int j=0;j<92;j++)
                        {
                            s1 = Long.toString(copy[j]);
                            out1.write(s1);
                            out1.newLine();
                        }
                        k++;
                        out1.close();
                        
                        
                        while(k<=y)
                        {
                            String s2;
                            BufferedWriter out2 =  new BufferedWriter(new FileWriter("enc.txt",true));
                            for(int j=0;j<92;j++)
                            {
                                s2 = Long.toString(copy[j]);
                                out2.write(s2);
                                out2.newLine();
                            }
                            k++;
                            out2.close();
                        }
                        
                        //Long[] fibonacci = new Long[a.length()];
                        // Open the file
                        
                        
                        FileInputStream fstream1 = new FileInputStream("enc.txt");
                        BufferedReader br2 = new BufferedReader(new InputStreamReader(fstream1));
                        
                        String strLine1;
                        int l=0;
                        while (l<a.length())   {
                            strLine1 = br2.readLine();
                            fibonacci[l] = Long.parseLong(strLine1);
                            l++;
                        }
                        br.close();
                        
                    }
                    
                    
                    Integer[] cipher_position = new Integer[a.length()];
                    for(int i=0; i<a.length(); i++){
                        //System.out.println(fibonacci(i));
                        cipher_position[i] = (int) ((fibonacci[i]+ Long.valueOf(position)-1)%96);//call the function fibonacci and add the position of key and do -1 bcs our fibonacci starts from 1 and we want to take the position of key also for calculating the cipher text position from array alphabets
                        //System.out.println(cipher_position[i-2]);
                    }
                    
                    String[] cipher_text = new String[a.length()];
                    for(int i=0; i<a.length(); i++)
                    {
                        cipher_text[i]=alphabets[cipher_position[i]];//storing the cipher text into cipher_text array
                    }
                    
                    
                    
                    Integer[] decimal_value = new Integer[a.length()];//array for storing ascii value of massage
                    
                    for(int i=0;i<a.length();i++)
                        decimal_value[i]=(int) a.charAt(i);
                    
                    String s1 = null;
                    Integer[] ascii_massage = new Integer[a.length()];
                    ascii_massage[0] = Math.abs(decimal_value[0] - 5*((int) cipher_text[0].charAt(0)));
                    s1 = String.valueOf(Character.toChars(ascii_massage[0]));
                    
                    for(int i=1;i<a.length();i++){
                        ascii_massage[i] = Math.abs(decimal_value[i] - 5*((int) cipher_text[i].charAt(0)));
                        //System.out.println(cipher_integer[i]);
                        
                        s1 += String.valueOf(Character.toChars(ascii_massage[i]));
                        
                    }
                    
                    setasccode(s1);
                    openfile of = new openfile();
                    of.setVisible(true);
                    
                    File f = new File("enc.txt");
                    f.delete();
                    
                    
                }
                
                else
                {
                    JOptionPane.showMessageDialog(null,"Please Enter Key First!!!");
                }
                
            } catch (IOException ex) {
                Logger.getLogger(main_page.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
        else
        {
            JOptionPane.showMessageDialog(null,"No File is Selected!!!");
        }
        
        
    }//GEN-LAST:event_db3ActionPerformed

    private void db2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_db2ActionPerformed
        // TODO add your handling code here:
        
        try {
            // TODO add your handling code here:
            if(!"".equals(dta1.getText()))
            {
            String[] alphabets = new String[96];//array for storing ascii value of upercase
            for(int i=0;i<96;i++)
                alphabets[i]=String.valueOf(Character.toChars(32+i));
            
            String a = dta1.getText();
            
            fibo(a.length());
            
            if(!"".equals(dta2.getText()))
            {
            String key = dta2.getText();
                      
            int position=0;
        
            while(!key.equals(alphabets[position]))//find the position of key in upercase string
            {
                position++;
            }
            
            Long[] fibonacci = new Long[a.length()];
            
            if(a.length()==1)
        {
            FileInputStream fstream = new FileInputStream("enc.txt");
        BufferedReader br = new BufferedReader(new InputStreamReader(fstream));

        String strLine;
        int f=0;
        strLine = br.readLine() ;
            fibonacci[f] = Long.parseLong(strLine);
            f++;
        
        br.close();
        }
            
            else if(a.length()<=92)
        {
         
        FileInputStream fstream = new FileInputStream("enc.txt");
        BufferedReader br = new BufferedReader(new InputStreamReader(fstream));

        String strLine;
        int f=0;
        while ((strLine = br.readLine()) != null)   {
            fibonacci[f] = Long.parseLong(strLine);
            f++;
        }
        br.close();
        }
        
        
        else
        {
            
            Long[] copy = new Long[92];
            
            FileInputStream fstream = new FileInputStream("enc.txt");
        BufferedReader br = new BufferedReader(new InputStreamReader(fstream));

        String strLine;
        int f=0;
        while (f!=92)   {
            strLine = br.readLine();
            copy[f] = Long.parseLong(strLine);
            f++;
        }
        br.close();
            int k=0;
            int y = a.length()/92;
            
            String s1;
            BufferedWriter out1 =  new BufferedWriter(new FileWriter("enc.txt"));
                for(int j=0;j<92;j++)
                {
                    s1 = Long.toString(copy[j]);
                    out1.write(s1);
                    out1.newLine();
                }
            k++;
            out1.close();
            
            
            while(k<=y)
            {
                String s2;
            BufferedWriter out2 =  new BufferedWriter(new FileWriter("enc.txt",true));
                for(int j=0;j<92;j++)
                {
                    s2 = Long.toString(copy[j]);
                    out2.write(s2);
                    out2.newLine();
                }
            k++;
            out2.close();
            }
            
            //Long[] fibonacci = new Long[a.length()];
        // Open the file
        
        
        FileInputStream fstream1 = new FileInputStream("enc.txt");
        BufferedReader br2 = new BufferedReader(new InputStreamReader(fstream1));

        String strLine1;
        int l=0;
        while (l<a.length())   {
            strLine1 = br2.readLine();
            fibonacci[l] = Long.parseLong(strLine1);
            l++;
        }
        br.close();
              
        }
            
            
        Integer[] cipher_position = new Integer[a.length()];
        for(int i=0; i<a.length(); i++){
            //System.out.println(fibonacci(i));
            cipher_position[i] = (int) ((fibonacci[i]+ Long.valueOf(position)-1)%96);//call the function fibonacci and add the position of key and do -1 bcs our fibonacci starts from 1 and we want to take the position of key also for calculating the cipher text position from array alphabets
            //System.out.println(cipher_position[i-2]);
        }
        
        String[] cipher_text = new String[a.length()];
        for(int i=0; i<a.length(); i++)
        {
            cipher_text[i]=alphabets[cipher_position[i]];//storing the cipher text into cipher_text array
        }
            
        
        
        Integer[] decimal_value = new Integer[a.length()];//array for storing ascii value of massage
        
        for(int i=0;i<a.length();i++)
            decimal_value[i]=(int) a.charAt(i);
        
        dta4.setText("");
         Integer[] ascii_massage = new Integer[a.length()];
        for(int i=0;i<a.length();i++){
            ascii_massage[i] = Math.abs(decimal_value[i] - 5*((int) cipher_text[i].charAt(0)));
             //System.out.println(cipher_integer[i]);
            
            dta4.setText(dta4.getText() + String.valueOf(Character.toChars(ascii_massage[i])));
            
        }   
            
            File f = new File("enc.txt");
            f.delete();
            }
            else
                {
                    JOptionPane.showMessageDialog(null,"Please Enter Key First!!!");
                }
            }
            else
            {
                JOptionPane.showMessageDialog(null,"Please Enter Your Encrypted Message!!!");
            }
        } catch (IOException ex) {
            Logger.getLogger(main_page.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }//GEN-LAST:event_db2ActionPerformed

    private void dta1FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_dta1FocusGained
        // TODO add your handling code here:
        dta1.setText("");
    }//GEN-LAST:event_dta1FocusGained

    private void jLabel1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel1MouseClicked
        // TODO add your handling code here:
        jLabel3.setVisible(false);
        jLabel4.setVisible(false);
        jLabel5.setVisible(false);
        jLabel10.setVisible(false);
        c=0;
        jLabel7.setVisible(false);
        jLabel8.setVisible(false);
        jLabel9.setVisible(false);
        d=0;
    }//GEN-LAST:event_jLabel1MouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(main_page.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        /* Create and display the form */
        
        java.awt.EventQueue.invokeLater(() -> {
            new main_page().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton b1;
    private javax.swing.JButton b2;
    private javax.swing.JButton b3;
    private javax.swing.JLabel closeB;
    private javax.swing.JButton db2;
    private javax.swing.JButton db3;
    private javax.swing.JButton dc;
    private javax.swing.JScrollPane dsp1;
    private javax.swing.JScrollPane dsp2;
    private javax.swing.JTextArea dta1;
    private javax.swing.JTextField dta2;
    private javax.swing.JTextField dta3;
    private javax.swing.JTextArea dta4;
    private javax.swing.JButton ec;
    private javax.swing.JLabel framedrag;
    private javax.swing.JLabel head;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel miniB;
    private javax.swing.JTextField path;
    private javax.swing.JScrollPane sp1;
    private javax.swing.JScrollPane sp2;
    private javax.swing.JTextArea ta1;
    private javax.swing.JTextField ta2;
    private javax.swing.JTextArea ta4;
    // End of variables declaration//GEN-END:variables

}

